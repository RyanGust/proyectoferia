import "./App.css";
import { Body } from "./app/components/Body";
import { Cabecera } from "./app/components/Cabecera";
import { Footer } from "./app/components/Footer";

function App() {
  return (
    <div className="Container">
    <Cabecera />
    <Body />
    <Footer />
    </div>
  );
}

export default App;
