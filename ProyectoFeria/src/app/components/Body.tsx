import cat from "../../assets/images/gato.png";
import dog from "../../assets/images/perro.png"
import pets from "../../assets/images/mascotas.png"
import besti from "../../assets/images/besties.png"
import guau from "../../assets/images/guaumor.png"
import pedi from "../../assets/images/pedigree.png"
import nutri from "../../assets/images/nutrique.png"
import nutriquecat from "../../assets/images/nutriquegatos.png"
import hill from "../../assets/images/hill.png"
import equi from "../../assets/images/equilibrio.png"
import nutrecat from "../../assets/images/nutrecat.png"
export const Body=()=>{
    return(
        <div>
            <main>
<div id="carouselExampleDark" className="carousel carousel-dark slide" data-bs-ride="carousel">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div className="carousel-inner">
    <div className="carousel-item active" data-bs-interval="5000">
      <img src={cat} className=" w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <div className="home">
          <h2 className="heading">Productos</h2>
          <p className="parrafo">Productos variados para animales domesticos</p>
          <p>  
            <a href="/#card" className="button">Lista de productos</a>
            <a href="/#" className="button">Carrito de compras</a>
          </p>
        </div>
      </div>
    </div>
    <div className="carousel-item" data-bs-interval="5000">
      <img src={dog} className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <div className="home">
          <h2 className="heading">Productos</h2>
          <p className="parrafo">Productos variados para animales domesticos</p>
          <p>  
            <a href="/#card" className="button">Lista de productos</a>
            <a href="/#" className="button">Carrito de compras</a>
          </p>
        </div>
      </div>
    </div>
    <div className="carousel-item" data-bs-interval="5000">
      <img src={pets} className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <div className="home">
          <h2 className="heading">Productos</h2>
          <p className="parrafo">Productos variados para animales domesticos</p>
          <p>  
            <a href="/#card" className="button">Lista de productos</a>
            <a href="/#" className="button">Carrito de compras</a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>

<section className="card" id="card">

    <h1 className="heading"> nuestros productos</h1>

    <div className="box-container">

        <div className="swiper-slide box">
            <img src={guau} alt="" />
            <h3>Guaumor - Cachorros raza mediana y grandes</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={besti} alt="" />
            <h3>Besties - Alimento Perros Cachorros Sabor Carne y Pollo</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={pedi} alt="" />
            <h3>Pedigree - Alimento Para Perro Adulto Raza Pequeña</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={nutri} alt="" />
            <h3>Nutrique - Perro Cachorro Talla Grande</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={hill} alt="" />
            <h3>Hills - Science Diet Adult Hairball Control Cat</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={equi} alt="" />
            <h3>Equilibrio - Veterinary O&D Obesity & Diabetic Gatos</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={nutriquecat} alt="" />
            <h3>Nutrique - Gato Adulto Castrado Control Peso</h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>

        <div className="swiper-slide box">
        <img src={nutrecat} alt="" />
            <h3>Nutrecat - Alimento Gatos Liberty<br/><br/></h3>
            <div className="price"> $500 - $1.000 </div>
            <div className="stars">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star-half-alt"></i>
            </div>
            <a href="/#" className="button">Agregar al carrito</a>
        </div>
    </div>
</section>
</main>
        </div>
    );

};