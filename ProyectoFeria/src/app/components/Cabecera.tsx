import Logo from "../../assets/images/logo.png";
export const Cabecera = () => {
  return (
    <div>
      <header className="header">
        <a href="" className="logo">
          {" "}
          <img src={Logo} alt="" />
        </a>
        <nav className="navbar">
          <a href="#">Inicio</a>
          <a href="#card">Productos</a>
          <a href="#">Servicios</a>
          <a href="#">Sobre Nosotros</a>
          <a href="#">Contacto</a>
        </nav>

        <div className="icons">
        <div className="fas fa-shopping-cart" id="cart-btn"></div>
        <div className="fas fa-user" id="login-btn"></div>
    </div>
      </header>
    </div>
  );
};
