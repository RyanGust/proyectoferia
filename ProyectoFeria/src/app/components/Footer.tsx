export const Footer=()=>{
    return(
        <div>

    <div className="contenedor">
        <h1>Formulario Contacto</h1>
        <div className="contenido">
            <div className="info">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui eligendi est voluptates quae ipsam eius, in quasi odio porro aut vel beatae aperiam.</p>
                <a href="#"><i className="tel fa fa-map-marked"></i> Ciudad, Pais</a>
                <a href="#"><i className="tel fa fa-phone"></i> 123-456-789</a>
                <a href="#"><i className="tel fa fa-envelope"></i> email@tudominio.com</a>

                <div className="redes-s">
                    <a href="#"><i className="fab fa-facebook-f"></i></a>
                    <a href="#"><i className="fab fa-twitter"></i></a>
                    <a href="#"><i className="fab fa-instagram"></i></a>
                </div>
            </div>

            <form action="enviado.html" method="GET" className="formulario">
                <input type="text" name="nombre" placeholder="Nombre" id="nombre"/>
                <input type="text" name="correo" placeholder="Correo electróico" id="correo"/>
                <input type="text" name="asunto" placeholder="Asunto" id="asunto"/>
                <textarea name="mensaje" id="mensaje" placeholder="Mensaje..."></textarea>
                <button type="button">Enviar Mensaje</button>
            </form>
        </div>
    </div>

    <footer className="footer">
    <ul className="nav justify-content-center border-bottom pb-3 mb-3">
      <li className="nav-item"><a href="/#" className="nav-link px-2 text-muted">Inicio</a></li>
      <li className="nav-item"><a href="/#card" className="nav-link px-2 text-muted">Producto</a></li>
      <li className="nav-item"><a href="/#" className="nav-link px-2 text-muted">Preguntas Frecuentes</a></li>
      <li className="nav-item"><a href="/#" className="nav-link px-2 text-muted">Sobre Nosotros</a></li>
    </ul>
    <p className="text-center text-muted">© 2022 Company, Inc</p>
  </footer>           
        </div>
    );

    
};