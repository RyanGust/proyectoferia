import {Routes, Route} from "react-router-dom";
import { Body } from "../../components/Body";
import { Cabecera } from "../../components/Cabecera";
import { Footer } from "../../components/Footer";

export const Ruteo = () => {
    return(
       <Routes>
        <Route path="/casita" element={<Cabecera/>} />
        <Route path="/cuerpo" element={<Body/>} />
        <Route path="/pie" element={<Footer/>} />
       </Routes>

    );
};